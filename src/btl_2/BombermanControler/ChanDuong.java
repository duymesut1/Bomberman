package btl_2.BombermanControler;

import btl_2.thucthe.Entity;

import static btl_2.BombermanGame.*;

public class ChanDuong {

    public static boolean chanDuong_Bom_duoi(Entity e, int p) {
        boolean result = checkIndex[e.getX() / 32][e.getY() / 32 + 1 + p] == 0 || checkIndex[e.getX() / 32][e.getY() / 32 + 1 + p] == 3
                || checkIndex[e.getX() / 32][e.getY() / 32 + 1 + p] == 6 || checkIndex[e.getX() / 32][e.getY() / 32 + 1 + p] == 7
                || checkIndex[e.getX() / 32][e.getY() / 32 + 1 + p] == 8;
        return result;
    }

    public static boolean chanDuong_Bom_tren(Entity e, int p) {
        boolean result = checkIndex[e.getX() / 32][e.getY() / 32 - 1 - p] == 0 || checkIndex[e.getX() / 32][e.getY() / 32 - 1 - p] == 3
                || checkIndex[e.getX() / 32][e.getY() / 32 - 1 - p] == 6 || checkIndex[e.getX() / 32][e.getY() / 32 - 1 - p] == 7
                || checkIndex[e.getX() / 32][e.getY() / 32 - 1 - p] == 8;
        return result;
    }

    public static boolean chanDuong_Bom_trai(Entity e, int p) {
        boolean result = checkIndex[e.getX() / 32 - 1 - p][e.getY() / 32] == 0 || checkIndex[e.getX() / 32 - 1 - p][e.getY() / 32] == 3
                || checkIndex[e.getX() / 32 - 1 - p][e.getY() / 32] == 6 || checkIndex[e.getX() / 32 - 1 - p][e.getY() / 32] == 7
                || checkIndex[e.getX() / 32 - 1 - p][e.getY() / 32] == 8;
        return result;
    }

    public static boolean chanDuong_Bom_phai(Entity e, int p) {
        boolean result = checkIndex[e.getX() / 32 + 1 + p][e.getY() / 32] == 0 || checkIndex[e.getX() / 32 + 1 + p][e.getY() / 32] == 3
                || checkIndex[e.getX() / 32 + 1 + p][e.getY() / 32] == 6 || checkIndex[e.getX() / 32 + 1 + p][e.getY() / 32] == 7
                || checkIndex[e.getX() / 32 + 1 + p][e.getY() / 32] == 8;
        return result;
    }

    public static boolean chanDuong_duoi(Entity e) {

        boolean result = checkIndex[e.getX() / 32][e.getY() / 32 + 1] == 0;
        return result;
    }

    public static boolean chanDuong_tren(Entity e) {
        boolean result = checkIndex[e.getX() / 32][e.getY() / 32 - 1] == 0;
        return result;
    }

    public static boolean chaDuong_trai(Entity e) {
        boolean result = checkIndex[e.getX() / 32 - 1][e.getY() / 32] == 0;
        return result;
    }

    public static boolean chanDuong_phai(Entity e) {
        boolean result = checkIndex[e.getX() / 32 + 1][e.getY() / 32] == 0;
        return result;
    }


}
