package btl_2.BombermanControler;

import btl_2.thucthe.doituongdong.*;
import btl_2.dohoa.Sprite;

import static btl_2.thucthe.item.SpeedItem.*;

public class DiChuyen {
    private static void datHuongDiChuyen(String huongDiChuyen, DoiTuong a, int isMove) {
        switch (huongDiChuyen) {
            case "down":
                diChuyen_xuong(a);
                a.setY(a.getY() + isMove);
                break;
            case "up":
                diChuyen_len(a);
                a.setY(a.getY() - isMove);
                break;
            case "left":
                diChuyen_trai(a);
                a.setX(a.getX() - isMove);
                break;
            case "right":
                diChuyen_phai(a);
                a.setX(a.getX() + isMove);
                break;
        }
    }

    public static void checkRun(DoiTuong a) {
        if (a instanceof Bomber && a.getCountStepOfAJump() > 0) {
            datHuongDiChuyen(a.getDirectionOfPlayer(), a, 8 * speed);
            a.setCountStepOfAJump(a.getCountStepOfAJump() - 1);
        }
        if ((a instanceof Ballom || a instanceof Oneal
                || a instanceof Doll || a instanceof Kondoria)
                && a.getCountStepOfAJump() > 0) {
            datHuongDiChuyen(a.getDirectionOfPlayer(), a, 4);
            a.setCountStepOfAJump(a.getCountStepOfAJump() - 1);
        }
    }

    public static void huongLen(DoiTuong a) {
        if (a.getY() % 32 == 0 && a.getX() % 32 == 0) {
            if (a instanceof Bomber && ChanDuong.chanDuong_tren(a)) {
                a.setDirectionOfPlayer("up");
                a.setCountStepOfAJump(4 / speed);
                checkRun(a);
            }
            if ((a instanceof Ballom || a instanceof Oneal || a instanceof Doll)
                    && ChanDuong.chanDuong_tren(a)) {
                a.setDirectionOfPlayer("up");
                a.setCountStepOfAJump(8);
                checkRun(a);
            }
        }
    }

    private static void diChuyen_len(DoiTuong a) {
        if (a instanceof Bomber && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.player_up.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.player_up_1.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.player_up.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.player_up_2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Ballom && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.ballom_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.ballom_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.ballom_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.ballom_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Oneal && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.oneal_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.oneal_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.oneal_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.oneal_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Doll && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.doll_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.doll_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.doll_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.doll_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
    }

    public static void huongXuong(DoiTuong a) {
        if (a.getY() % 32 == 0 && a.getX() % 32 == 0) {
            if (a instanceof Bomber && ChanDuong.chanDuong_duoi(a)) {
                a.setDirectionOfPlayer("down");
                a.setCountStepOfAJump(4 / speed);
                checkRun(a);
            }
            if ((a instanceof Ballom || a instanceof Oneal || a instanceof Doll)
                    && ChanDuong.chanDuong_duoi(a)) {
                a.setDirectionOfPlayer("down");
                a.setCountStepOfAJump(8);
                checkRun(a);
            }
        }
    }

    private static void diChuyen_xuong(DoiTuong a) {
        if (a instanceof Bomber && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.player_down.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.player_down_1.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.player_down.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.player_down_2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Ballom && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.ballom_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.ballom_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.ballom_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.ballom_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Oneal && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.oneal_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.oneal_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.oneal_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.oneal_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Doll && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.doll_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.doll_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.doll_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.doll_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
    }



    public static void huongTrai(DoiTuong a) {
        if (a.getX() % 32 == 0 && a.getY() % 32 == 0) {
            if (a instanceof Bomber && ChanDuong.chaDuong_trai(a)) {
                a.setDirectionOfPlayer("left");
                a.setCountStepOfAJump(4 / speed);
                checkRun(a);
            }
            if ((a instanceof Ballom || a instanceof Oneal
                    || a instanceof Doll || a instanceof Kondoria)
                    && ChanDuong.chaDuong_trai(a)) {
                a.setDirectionOfPlayer("left");
                a.setCountStepOfAJump(8);
                checkRun(a);
            }
        }
    }

    private static void diChuyen_trai(DoiTuong a) {
        if (a instanceof Bomber && a.getX() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.player_left.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.player_left_1.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.player_left.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.player_left_2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Ballom && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.ballom_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.ballom_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.ballom_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.ballom_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Oneal && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.oneal_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.oneal_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.oneal_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.oneal_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Doll && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.doll_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.doll_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.doll_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.doll_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Kondoria && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.kondoria_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.kondoria_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.kondoria_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.kondoria_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
    }

    public static void huongPhai(DoiTuong a) {
        if (a.getX() % 32 == 0 && a.getY() % 32 == 0) {
            if (a instanceof Bomber && ChanDuong.chanDuong_phai(a)) {
                a.setDirectionOfPlayer("right");
                a.setCountStepOfAJump(4 / speed);
                checkRun(a);
            }
            if ((a instanceof Ballom || a instanceof Oneal
                    || a instanceof Doll || a instanceof Kondoria)
                    && ChanDuong.chanDuong_phai(a)) {
                a.setDirectionOfPlayer("right");
                a.setCountStepOfAJump(8);
                checkRun(a);
            }
        }
    }

    public static void diChuyen_phai(DoiTuong a) {
        if (a instanceof Bomber && a.getX() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.player_right.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.player_right_1.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.player_right.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.player_right_2.getFxImage());
                a.setSwapImg(1);
            }
        }

        if (a instanceof Ballom && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.ballom_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.ballom_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.ballom_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.ballom_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Oneal && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.oneal_left1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.oneal_left2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.oneal_left3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.oneal_left2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Doll && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.doll_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.doll_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.doll_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.doll_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
        if (a instanceof Kondoria && a.getY() % 8 == 0) {
            if (a.getSwapImg() == 1) {
                a.setImg(Sprite.kondoria_right1.getFxImage());
                a.setSwapImg(2);
            } else if (a.getSwapImg() == 2) {
                a.setImg(Sprite.kondoria_right2.getFxImage());
                a.setSwapImg(3);
            } else if (a.getSwapImg() == 3) {
                a.setImg(Sprite.kondoria_right3.getFxImage());
                a.setSwapImg(4);
            } else {
                a.setImg(Sprite.kondoria_right2.getFxImage());
                a.setSwapImg(1);
            }
        }
    }
}
