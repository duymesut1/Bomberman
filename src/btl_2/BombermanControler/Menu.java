package btl_2.BombermanControler;

import javafx.scene.Group;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import btl_2.manchoi.*;

import static btl_2.BombermanGame.*;

public class Menu {
    public static int bombNumber = 20, timeNumber = 120;
    private static ImageView status_game;
    public static Text levels, bom, times;

    public static void capNhat_menu() {
        levels.setText("Level: " + bom_Level);
        bom.setText("Bombs: " + bombNumber);

        if (doiTuong.isLifeOfEnemy())
            if (isRun) {
                Image pauseGame = new Image("images/pauseGame.png");
                status_game.setImage(pauseGame);
            } else {
                Image playGame = new Image("images/playGame.png");
                status_game.setImage(playGame);
            }
        else {
            Image newGame = new Image("images/newGame.png");
            status_game.setImage(newGame);
        }
    }

    public static void createMenu(Group root) {
        levels = new Text("Level: 1");
        levels.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        levels.setFill(Color.WHITE);
        levels.setX(416);
        levels.setY(20);

        bom = new Text("Bombs: 20");
        bom.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        bom.setFill(Color.WHITE);
        bom.setX(512);
        bom.setY(20);

        times = new Text("Times: 120");
        times.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        times.setFill(Color.WHITE);
        times.setX(608);
        times.setY(20);

        Image newGame = new Image("images/newGame.png");
        status_game = new ImageView(newGame);
        status_game.setX(-75);
        status_game.setY(-10);
        status_game.setScaleX(0.5);
        status_game.setScaleY(0.5);

        Pane pane = new Pane();
        pane.getChildren().addAll(levels, bom, times, status_game);
        pane.setMinSize(800, 32);
        pane.setMaxSize(800, 480);
        pane.setStyle("-fx-background-color: #353535");

        root.getChildren().add(pane);

        status_game.setOnMouseClicked(event -> {
            if (doiTuong.isLifeOfEnemy()) {
                isRun = !isRun;
            } else {
                new Man1();
                isRun = true;
            }
            capNhat_menu();
        });

    }


}
