package btl_2.thucthe.item;

import javafx.scene.image.Image;
import btl_2.thucthe.Entity;
import btl_2.thucthe.doituongtinh.Bomb;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.*;

public class FlameItem extends Items {

    public FlameItem(int x, int y, Image img) {
        super(x, y, img);
    }

    public FlameItem(boolean received) {
        super(received);
    }

    public FlameItem() {
    }

    @Override
    public void update() {
        for (Entity entity : vatCan)
            if (entity instanceof FlameItem && !this.received)
                if (listKills[entity.getX() / 32][entity.getY() / 32] == 4)
                    entity.setImg(Sprite.powerup_flames.getFxImage());

        if (!this.received)
            if (doiTuong.getX() == this.x && doiTuong.getY() == this.y) {
                this.setImg(Sprite.grass.getFxImage());
                this.received = true;
                Bomb.power_Bom += 2;
            }
    }
}
