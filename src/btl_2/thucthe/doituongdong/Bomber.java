package btl_2.thucthe.doituongdong;

import btl_2.BombermanGame;
import javafx.scene.image.Image;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.*;

public class Bomber extends DoiTuong {
    public static int killSwap = 1;
    private static int totalKill = 0;

    public Bomber(int x, int y, Image img) {
        super(x, y, img);
    }

    private void checkBombs() {
        if (listKills[doiTuong.getX() / 32][doiTuong.getY() / 32] == 4)
            doiTuong.setLifeOfEnemy(false);
    }

    private void killBomber(DoiTuong doiTuong) {
        if (totalKill % 16 == 0) {
            if (killSwap == 1) {
                doiTuong.setImg(Sprite.player_dead1.getFxImage());
                killSwap = 2;
            } else if (killSwap == 2) {
                doiTuong.setImg(Sprite.player_dead2.getFxImage());
                killSwap = 3;
            } else if (killSwap == 3) {
                doiTuong.setImg(Sprite.player_dead3.getFxImage());
                killSwap = 4;
            }else {
                doiTuong.setImg(Sprite.transparent.getFxImage());
                isRun = false;
                Image gameOver = new Image("images/gameover.jpg");
                home_view.setImage(gameOver);
            }
        }
    }

    private void checkEnemy3() {
        int ax = doiTuong.getX();
        int ay = doiTuong.getY();
        for (DoiTuong doiTuong : enemies) {
            int bx = doiTuong.getX();
            int by = doiTuong.getY();
            if (ax == bx && by - 32 <= ay && by + 32 >= ay
                    || ay == by && bx - 32 <= ax && bx + 32 >= ax) {
                BombermanGame.doiTuong.lifeOfEnemy = false;
                break;
            }
        }
    }

    @Override
    public void update() {
        checkBombs();
        checkEnemy3();
        totalKill++;
        if (!doiTuong.lifeOfEnemy)
            killBomber(doiTuong);
    }
}
