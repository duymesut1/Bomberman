package btl_2.thucthe.doituongdong;

import btl_2.dohoa.Sprite;
import javafx.scene.image.Image;
import btl_2.BombermanControler.DiChuyen;
import java.util.Random;
import static btl_2.BombermanGame.*;

public class Ballom extends DoiTuong {
    private static int killSwap = 1;
    private static int totalKill = 0;

    public Ballom(int x, int y, Image img) {
        super(x, y, img);
    }

    private void kill() {
        for (DoiTuong doiTuong : enemies) {
            if (listKills[doiTuong.getX() / 32][doiTuong.getY() / 32] == 4) {
                doiTuong.setLifeOfEnemy(false);
            }
        }
    }

    private void killBallom(DoiTuong doiTuong) {
        if (totalKill % 16 == 0) {
            if (killSwap == 1) {
                doiTuong.setImg(Sprite.mob_dead1.getFxImage());
                killSwap = 2;
            } else if (killSwap == 2) {
                doiTuong.setImg(Sprite.mob_dead2.getFxImage());
                killSwap = 3;
            } else if (killSwap == 3) {
                doiTuong.setImg(Sprite.mob_dead3.getFxImage());
                killSwap = 4;
            } else {
                doiTuong.setLifeOfEnemy(false);
                enemies.remove(doiTuong);
                killSwap = 1;
            }
        }
    }

    @Override
    public void update() {
        kill();
        totalKill++;
        for (DoiTuong doiTuong : enemies) {
            if (doiTuong instanceof Ballom && !doiTuong.lifeOfEnemy)
                killBallom(doiTuong);
        }

        if (this.y % 16 == 0 && this.x % 16 == 0) {
            Random random = new Random();
            int dir = random.nextInt(4);
            switch (dir) {
                case 0:
                    DiChuyen.huongXuong(this);
                    break;
                case 1:
                    DiChuyen.huongLen(this);
                    break;
                case 2:
                    DiChuyen.huongTrai(this);
                    break;
                case 3:
                    DiChuyen.huongPhai(this);
                    break;
            }
        }
    }
}
