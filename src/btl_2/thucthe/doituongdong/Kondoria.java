package btl_2.thucthe.doituongdong;

import javafx.scene.image.Image;
import btl_2.BombermanControler.DiChuyen;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.*;

public class Kondoria extends DoiTuong {

    private static int killSwap = 1;
    private static int totalKill = 0;
    private static boolean dir;

    public Kondoria(int x, int y, Image img) {
        super(x, y, img);
    }


    private void checkKillKondoria(DoiTuong doiTuong) {
        if (totalKill % 16 == 0) {
            if (killSwap == 1) {
                doiTuong.setImg(Sprite.kondoria_dead.getFxImage());
                killSwap = 2;
            } else if (killSwap == 2) {
                doiTuong.setImg(Sprite.player_dead3.getFxImage());
                killSwap = 3;
            } else {
                doiTuong.setLifeOfEnemy(false);
                enemies.remove(doiTuong);
                killSwap = 1;
            }
        }
    }

    @Override
    public void update() {
        totalKill++;
        for (DoiTuong doiTuong : enemies) {
            if (doiTuong instanceof Kondoria && !doiTuong.lifeOfEnemy)
                checkKillKondoria(doiTuong);
        }

        if (this.y % 16 == 0 && this.x % 16 == 0) {
            if (this.x / 32 <= 1 || this.x / 32 >= bom_Width - 2)
                dir = !dir;

            if (dir)
                DiChuyen.huongTrai(this);
            else
                DiChuyen.huongPhai(this);
        }
    }
}
