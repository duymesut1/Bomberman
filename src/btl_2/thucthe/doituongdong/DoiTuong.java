package btl_2.thucthe.doituongdong;

import javafx.scene.image.Image;
import btl_2.thucthe.Entity;

public abstract class DoiTuong extends Entity {
    protected int isMove;
    protected int swapImg;
    protected String directionOfPlayer;
    protected int countStepOfAJump;
    protected int countToRun;
    protected boolean lifeOfEnemy;

    public DoiTuong(int xUnit, int yUnit, Image img) {
        super(xUnit, yUnit, img);
    }

    public DoiTuong(int isMove, int swapImg, String directionOfPlayer, int countStepOfAJump, int countToRun) {
        this.isMove = isMove;
        this.swapImg = swapImg;
        this.directionOfPlayer = directionOfPlayer;
        this.countStepOfAJump = countStepOfAJump;
        this.countToRun = countToRun;
    }

    public DoiTuong(boolean lifeOfEnemy) {
        this.lifeOfEnemy = lifeOfEnemy;
    }

    public boolean isLifeOfEnemy() {
        return lifeOfEnemy;
    }

    public void setLifeOfEnemy(boolean lifeOfEnemy) {
        this.lifeOfEnemy = lifeOfEnemy;
    }

    public int getSwapImg() {
        return swapImg;
    }

    public void setSwapImg(int swapImg) {
        this.swapImg = swapImg;
    }

    public String getDirectionOfPlayer() {
        return directionOfPlayer;
    }

    public void setDirectionOfPlayer(String directionOfPlayer) {
        this.directionOfPlayer = directionOfPlayer;
    }

    public int getCountStepOfAJump() {
        return countStepOfAJump;
    }

    public void setCountStepOfAJump(int countStepOfAJump) {
        this.countStepOfAJump = countStepOfAJump;
    }

    public int getCountToRun() {
        return countToRun;
    }

    public void setCountToRun(int countToRun) {
        this.countToRun = countToRun;
    }

    public DoiTuong() {

    }

    @Override
    public void update() {

    }
}
