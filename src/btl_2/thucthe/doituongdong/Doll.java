package btl_2.thucthe.doituongdong;

import javafx.scene.image.Image;
import btl_2.BombermanControler.DiChuyen;
import btl_2.thucthe.doituongdong.thuattoan.AStar;
import btl_2.thucthe.doituongdong.thuattoan.Node;
import btl_2.dohoa.Sprite;

import java.util.List;

import static btl_2.BombermanGame.*;

public class Doll extends DoiTuong {
    private static int killSwap = 1;
    private static int totalKill = 0;
    public Doll(int isMove, int swap, String direction, int count, int countToRun) {
        super(4, 1, "up", 0, 0);
    }

    public Doll(boolean lifeOfEnemy) {
        super(lifeOfEnemy);
    }

    public Doll() {
    }
    public Doll(int x, int y, Image img) {
        super(x, y, img);
    }

    private void checkDollKill(DoiTuong doiTuong) {
        if (totalKill % 16 == 0) {
            if (killSwap == 1) {
                doiTuong.setImg(Sprite.doll_dead.getFxImage());
                killSwap = 2;
            } else if (killSwap == 2) {
                doiTuong.setImg(Sprite.player_dead3.getFxImage());
                killSwap = 3;
            } else {
                doiTuong.setLifeOfEnemy(false);
                enemies.remove(doiTuong);
                killSwap = 1;
            }
        }
    }

    private void moveOfDoll() {
        if (this.x % 32 == 0 && this.y % 32 == 0) {
            Node initialNode = new Node(this.y / 32, this.x / 32);
            Node finalNode = new Node(doiTuong.getY() / 32, doiTuong.getX() / 32);

            int r = bom_Height;
            int c = bom_Width;

            AStar aStar = new AStar(r, c, initialNode, finalNode);

            int[][] blocksArray = new int[bom_Width * bom_Height][2];
            int countBlock = 0;

            for (int i = 0; i < bom_Height; i++)
                for (int j = 0; j < bom_Width; j++)
                    if (checkIndex[j][i] != 0) {
                        blocksArray[countBlock][0] = i;
                        blocksArray[countBlock][1] = j;
                        countBlock++;
                    }

            aStar.setBlocks(blocksArray, countBlock);
            List<Node> path = aStar.findPath();
            if (path.size() != 0) {
                int nextY = path.get(1).getRow();
                int nextX = path.get(1).getCol();

                if (this.y / 32 > nextY)
                    DiChuyen.huongLen(this);
                if (this.y / 32 < nextY)
                    DiChuyen.huongXuong(this);
                if (this.x / 32 > nextX)
                    DiChuyen.huongTrai(this);
                if (this.x / 32 < nextX)
                    DiChuyen.huongPhai(this);
            }
        }
    }

    @Override
    public void update() {
        totalKill++;
        for (DoiTuong doiTuong : enemies) {
            if (doiTuong instanceof Doll && !doiTuong.lifeOfEnemy)
                checkDollKill(doiTuong);
        }
        moveOfDoll();
    }
}
