package btl_2.thucthe.doituongdong;

import javafx.scene.image.Image;
import btl_2.BombermanControler.DiChuyen;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.*;

public class Oneal extends DoiTuong {
    private static int killSwap = 1;
    private static int totalKill = 0;

    public Oneal(int x, int y, Image img) {
        super(x, y, img);
    }


    private void killOneal(DoiTuong doiTuong) {
        if (totalKill % 16 == 0) {
            if (killSwap == 1) {
                doiTuong.setImg(Sprite.oneal_dead.getFxImage());
                killSwap = 2;
            } else if (killSwap == 2) {
                doiTuong.setImg(Sprite.player_dead3.getFxImage());
                killSwap = 3;
            } else {
                doiTuong.setLifeOfEnemy(false);
                enemies.remove(doiTuong);
                killSwap = 1;
            }
        }
    }

    @Override
    public void update() {
        totalKill++;
        for (DoiTuong doiTuong : enemies) {
            if (doiTuong instanceof Oneal && !doiTuong.lifeOfEnemy)
                killOneal(doiTuong);
        }

        if (this.y % 16 == 0 && this.x % 16 == 0) {
            if (doiTuong.getX() < this.x) {
                DiChuyen.huongTrai(this);
            }
            if (doiTuong.getX() > this.x) {
                DiChuyen.huongPhai(this);
            }
            if (doiTuong.getY() > this.y) {
                DiChuyen.huongXuong(this);
            }
            if (doiTuong.getY() < this.y) {
                DiChuyen.huongLen(this);
            }
        }
    }
}
