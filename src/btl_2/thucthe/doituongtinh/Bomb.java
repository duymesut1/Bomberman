package btl_2.thucthe.doituongtinh;

import btl_2.BombermanControler.ChanDuong;
import btl_2.thucthe.Entity;
import btl_2.dohoa.Sprite;
import btl_2.amthanh.SoundManager;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

import static btl_2.BombermanGame.*;
import static btl_2.BombermanControler.Menu.bombNumber;

public class Bomb extends Entity {
    private static final List<Entity> list_bom_middle_with = new ArrayList<>();
    private static final List<Entity> list_bom_middle_height = new ArrayList<>();

    private static long thoiGianBomNo;
    private static long thoiGianTmp;
    private static Entity bom;
    private static int activeSwap = 1;
    private static int explosionSwap = 1;

    public static int power_Bom = 0;

    private static int power_bom_down = 0;
    private static int power_bom_up = 0;
    private static int power_bom_left = 0;
    private static int power_bom_right = 0;

    private static Entity edgeDown = null;
    private static Entity edgeUp = null;
    private static Entity edgeLeft = null;
    private static Entity edgeRight = null;
    private static boolean is_edge = false;
    private static boolean is_middle = false;

    public static int is_bom = 0;

    public Bomb(int x, int y, Image img) {
        super(x, y, img);
    }

    public static void taoMiddle() {
        Entity middle;
        for (int i = 1; i <= power_bom_down; i++) {
            middle = new Bomb(bom.getX() / 32, bom.getY() / 32 + i,
                    Sprite.bomb_exploded.getFxImage());
            list_bom_middle_height.add(middle);
        }
        for (int i = 1; i <= power_bom_up; i++) {
            middle = new Bomb(bom.getX() / 32, bom.getY() / 32 - i,
                    Sprite.bomb_exploded.getFxImage());
            list_bom_middle_height.add(middle);
        }
        for (int i = 1; i <= power_bom_left; i++) {
            middle = new Bomb(bom.getX() / 32 - i, bom.getY() / 32,
                    Sprite.bomb_exploded.getFxImage());
            list_bom_middle_with.add(middle);
        }
        for (int i = 1; i <= power_bom_right; i++) {
            middle = new Bomb(bom.getX() / 32 + i, bom.getY() / 32,
                    Sprite.bomb_exploded.getFxImage());
            list_bom_middle_with.add(middle);
        }
        vatCan.addAll(list_bom_middle_with);
        vatCan.addAll(list_bom_middle_height);
    }

    public static void datBom() {
        if (is_bom == 0 && bombNumber > 0) {
            bombNumber--;
            is_bom = 1;
            thoiGianBomNo = System.currentTimeMillis();
            thoiGianTmp = thoiGianBomNo;
            int x = doiTuong.getX() / 32;
            int y = doiTuong.getY() / 32;
            x = Math.round(x);
            y = Math.round(y);
            bom = new Bomb(x, y, Sprite.bomb.getFxImage());
            vatCan.add(bom);
            checkIndex[doiTuong.getX() / 32][doiTuong.getY() / 32] = 4;
        }
    }

    public static void kichHoat_Bom() {
        if (activeSwap == 1) {
            bom.setImg(Sprite.bomb.getFxImage());
            activeSwap = 2;
        }
        if (activeSwap == 2) {
            bom.setImg(Sprite.bomb_1.getFxImage());
            activeSwap = 3;
        }
        if (activeSwap == 3) {
            bom.setImg(Sprite.bomb_2.getFxImage());
            activeSwap = 4;
        }
        {
            bom.setImg(Sprite.bomb_1.getFxImage());
            activeSwap = 1;
        }
    }

    public static void taoEdge() {
        if (ChanDuong.chanDuong_Bom_duoi(bom, 0)) {
            edgeDown = new Bomb(bom.getX() / 32, bom.getY() / 32 + 1,
                    Sprite.bomb_exploded.getFxImage());
            if (power_Bom > 0)
                for (int i = 1; i <= power_Bom; i++)
                    if (ChanDuong.chanDuong_Bom_duoi(bom, i)) {
                        edgeDown.setY(bom.getY() + 32 + i * 32);
                        power_bom_down++;
                    } else break;
            vatCan.add(edgeDown);
        }
        if (ChanDuong.chanDuong_Bom_tren(bom, 0)) {
            edgeUp = new Bomb(bom.getX() / 32, bom.getY() / 32 - 1,
                    Sprite.bomb_exploded.getFxImage());
            if (power_Bom > 0)
                for (int i = 1; i <= power_Bom; i++)
                    if (ChanDuong.chanDuong_Bom_tren(bom, i)) {
                        edgeUp.setY(bom.getY() - 32 - i * 32);
                        power_bom_up++;
                    } else break;
            vatCan.add(edgeUp);
        }
        if (ChanDuong.chanDuong_Bom_trai(bom, 0)) {
            edgeLeft = new Bomb(bom.getX() / 32 - 1, bom.getY() / 32,
                    Sprite.bomb_exploded.getFxImage());
            if (power_Bom > 0)
                for (int i = 1; i <= power_Bom; i++)
                    if (ChanDuong.chanDuong_Bom_trai(bom, i)) {
                        edgeLeft.setX(bom.getX() - 32 - i * 32);
                        power_bom_left++;
                    } else break;
            vatCan.add(edgeLeft);
        }
        if (ChanDuong.chanDuong_Bom_phai(bom, 0)) {
            edgeRight = new Bomb(bom.getX() / 32 + 1, bom.getY() / 32,
                    Sprite.bomb_exploded.getFxImage());
            if (power_Bom > 0)
                for (int i = 1; i <= power_Bom; i++)
                    if (ChanDuong.chanDuong_Bom_phai(bom, i)) {
                        edgeRight.setX(bom.getX() + 32 + i * 32);
                        power_bom_right++;
                    } else break;
            vatCan.add(edgeRight);
        }
    }


    private static void kiemTra_BomNo() {
        if (is_bom == 2)
            if (System.currentTimeMillis() - thoiGianBomNo < 1000) {
                if (System.currentTimeMillis() - thoiGianTmp > 100) {
                    if (!is_edge) {
                        taoEdge();
                        is_edge = true;
                    }
                    if (power_Bom > 0) {
                        if (!is_middle) {
                            taoMiddle();
                            is_middle = true;
                        }
                    }
                    new SoundManager("sound/bomb_explosion.wav", "explosion");
                    trungTam_VuNo();
                    thoiGianTmp += 100;
                }
            } else {
                is_bom = 0;
                checkIndex[bom.getX() / 32][bom.getY() / 32] = 0;
                listKills[bom.getX() / 32][bom.getY() / 32] = 0;
                bom.setImg(Sprite.transparent.getFxImage());
                if (ChanDuong.chanDuong_Bom_duoi(bom, power_bom_down)) {
                    edgeDown.setImg(Sprite.transparent.getFxImage());
                    checkIndex[edgeDown.getX() / 32][edgeDown.getY() / 32] = 0;
                    listKills[edgeDown.getX() / 32][edgeDown.getY() / 32] = 0;
                }
                if (ChanDuong.chanDuong_Bom_tren(bom, power_bom_up)) {
                    edgeUp.setImg(Sprite.transparent.getFxImage());
                    checkIndex[edgeUp.getX() / 32][edgeUp.getY() / 32] = 0;
                    listKills[edgeUp.getX() / 32][edgeUp.getY() / 32] = 0;
                }
                if (ChanDuong.chanDuong_Bom_trai(bom, power_bom_left)) {
                    edgeLeft.setImg(Sprite.transparent.getFxImage());
                    checkIndex[edgeLeft.getX() / 32][edgeLeft.getY() / 32] = 0;
                    listKills[edgeLeft.getX() / 32][edgeLeft.getY() / 32] = 0;
                }
                if (ChanDuong.chanDuong_Bom_phai(bom, power_bom_right)) {
                    edgeRight.setImg(Sprite.transparent.getFxImage());
                    checkIndex[edgeRight.getX() / 32][edgeRight.getY() / 32] = 0;
                    listKills[edgeRight.getX() / 32][edgeRight.getY() / 32] = 0;
                }
                if (is_middle) {
                    for (Entity e : list_bom_middle_with) {
                        listKills[e.getX() / 32][e.getY() / 32] = 0;
                        checkIndex[e.getX() / 32][e.getY() / 32] = 0;
                    }
                    for (Entity e : list_bom_middle_height) {
                        listKills[e.getX() / 32][e.getY() / 32] = 0;
                        checkIndex[e.getX() / 32][e.getY() / 32] = 0;
                    }
                }
                vatCan.removeAll(list_bom_middle_height);
                vatCan.removeAll(list_bom_middle_with);
                list_bom_middle_height.clear();
                list_bom_middle_with.clear();
                is_edge = false;
                is_middle = false;
                power_bom_down = 0;
                power_bom_up = 0;
                power_bom_left = 0;
                power_bom_right = 0;
            }
    }

    public static void trungTam_VuNo() {
        if (explosionSwap == 1) {
            bom.setImg(Sprite.bomb_exploded.getFxImage());
            listKills[bom.getX() / 32][bom.getY() / 32] = 4;
            if (ChanDuong.chanDuong_Bom_duoi(bom, power_bom_down)) {
                edgeDown.setImg(Sprite.explosion_vertical_down_last.getFxImage());
                listKills[edgeDown.getX() / 32][edgeDown.getY() / 32] = 4;
            }
            if (ChanDuong.chanDuong_Bom_tren(bom, power_bom_up)) {
                edgeUp.setImg(Sprite.explosion_vertical_top_last.getFxImage());
                listKills[edgeUp.getX() / 32][edgeUp.getY() / 32] = 4;
            }
            if (ChanDuong.chanDuong_Bom_trai(bom, power_bom_left)) {
                edgeLeft.setImg(Sprite.explosion_horizontal_left_last.getFxImage());
                listKills[edgeLeft.getX() / 32][edgeLeft.getY() / 32] = 4;
            }
            if (ChanDuong.chanDuong_Bom_phai(bom, power_bom_right)) {
                edgeRight.setImg(Sprite.explosion_horizontal_right_last.getFxImage());
                listKills[edgeRight.getX() / 32][edgeRight.getY() / 32] = 4;
            }
            if (list_bom_middle_height.size() > 0){
                for (Entity e : list_bom_middle_height) {
                    e.setImg(Sprite.explosion_vertical.getFxImage());
                    listKills[e.getX() / 32][e.getY() / 32] = 4;
                }}
            if (list_bom_middle_with.size() > 0){
                for (Entity e : list_bom_middle_with) {
                    e.setImg(Sprite.explosion_horizontal.getFxImage());
                    listKills[e.getX() / 32][e.getY() / 32] = 4;
                }}
            explosionSwap = 2;

        } else if (explosionSwap == 2) {
            bom.setImg(Sprite.bomb_exploded1.getFxImage());
            if (ChanDuong.chanDuong_Bom_duoi(bom, power_bom_down)) {
                edgeDown.setImg(Sprite.explosion_vertical_down_last1.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_tren(bom, power_bom_up)) {
                edgeUp.setImg(Sprite.explosion_vertical_top_last1.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_trai(bom, power_bom_left)) {
                edgeLeft.setImg(Sprite.explosion_horizontal_left_last1.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_phai(bom, power_bom_right)) {
                edgeRight.setImg(Sprite.explosion_horizontal_right_last1.getFxImage());
            }
            if (is_middle) {
                for (Entity e : list_bom_middle_height) {
                    e.setImg(Sprite.explosion_vertical1.getFxImage());
                }
                for (Entity e : list_bom_middle_with) {
                    e.setImg(Sprite.explosion_horizontal1.getFxImage());
                }
            }
            explosionSwap = 3;

        } else if (explosionSwap == 3) {
            bom.setImg(Sprite.bomb_exploded2.getFxImage());
            if (ChanDuong.chanDuong_Bom_duoi(bom, power_bom_down)) {
                edgeDown.setImg(Sprite.explosion_vertical_down_last2.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_tren(bom, power_bom_up)) {
                edgeUp.setImg(Sprite.explosion_vertical_top_last2.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_trai(bom, power_bom_left)) {
                edgeLeft.setImg(Sprite.explosion_horizontal_left_last2.getFxImage());
            }
            if (ChanDuong.chanDuong_Bom_phai(bom, power_bom_right)) {
                edgeRight.setImg(Sprite.explosion_horizontal_right_last2.getFxImage());
            }
            if (is_middle) {
                for (Entity e : list_bom_middle_height) {
                    e.setImg(Sprite.explosion_vertical2.getFxImage());
                }
                for (Entity e : list_bom_middle_with) {
                    e.setImg(Sprite.explosion_horizontal2.getFxImage());
                }
            }
            explosionSwap = 1;
        }
    }

    private static void kiemTraHoatDong_Bom() {
        if (is_bom == 1) {
            if (System.currentTimeMillis() - thoiGianBomNo < 2000) {
                if (System.currentTimeMillis() - thoiGianTmp > 100) {
                    kichHoat_Bom();
                    thoiGianTmp += 100;
                }
            } else {
                is_bom = 2;
                thoiGianBomNo = System.currentTimeMillis();
                thoiGianTmp = thoiGianBomNo;
            }
        }
    }


    @Override
    public void update() {
        kiemTraHoatDong_Bom();
        kiemTra_BomNo();
    }
}
