package btl_2.thucthe.doituongtinh;

import javafx.scene.image.Image;
import btl_2.thucthe.Entity;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.vatCan;
import static btl_2.BombermanGame.listKills;

public class Brick extends Entity {
    private void checkItem() {
        for (Entity e : vatCan) {
            if (e instanceof Brick)
                if (listKills[e.getX() / 32][e.getY() / 32] == 4) {
                    e.setImg(Sprite.grass.getFxImage());
                }
        }
    }

    public Brick(int x, int y, Image img) {
        super(x, y, img);
    }

    @Override
    public void update() {
        checkItem();
    }
}
