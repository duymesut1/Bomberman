package btl_2.amthanh;

import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;
import javax.swing.*;

import static btl_2.BombermanGame.doiTuong;
import static btl_2.manchoi.NextLevel.isWait;

public class SoundManager extends JFrame {
    public static boolean isDied;
    public static boolean isTitle;
    private static boolean isComplete;
    public static Clip title;
    public static Clip bomb;
    public static Clip died;

    public SoundManager(String name, String sound) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            URL url = this.getClass().getClassLoader().getResource(name);

            assert url != null;
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            if (sound.equals("explosion")) {
                bomb = AudioSystem.getClip();
                bomb.open(audioIn);
                bomb.start();
            } else if (sound.equals("title")) {
                title = AudioSystem.getClip();
                title.open(audioIn);
                title.start();
                title.loop(10);
            } else if (sound.equals("default")) {
                Clip clip = AudioSystem.getClip();
                clip.open(audioIn);
                clip.start();
            } else if (sound.equals("just_died")) {
                died = AudioSystem.getClip();
                died.open(audioIn);
                died.start();
            }
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public static void updateSound() {

        if (!doiTuong.isLifeOfEnemy()) {
            title.close();
            bomb.close();
            if (!isDied) {
                new SoundManager("sound/just_died.wav", "just_died");
                isDied = true;
            }
        }
        if (!isTitle) {
            new SoundManager("sound/title_screen.wav", "title");
            isTitle = true;
        }
        if (isWait) {
            title.close();
            bomb.close();
            if (!isComplete) {
                new SoundManager("sound/level_complete.wav", "default");
                isComplete = true;
            }
        }
    }
}
