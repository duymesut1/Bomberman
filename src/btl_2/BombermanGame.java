package btl_2;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import btl_2.BombermanControler.Menu;
import btl_2.BombermanControler.DiChuyen;
import btl_2.thucthe.Entity;
import btl_2.thucthe.doituongdong.DoiTuong;
import btl_2.thucthe.doituongdong.Bomber;
import btl_2.thucthe.doituongtinh.Bomb;
import btl_2.thucthe.doituongtinh.Portal;
import btl_2.dohoa.Sprite;

import java.util.ArrayList;
import java.util.List;

import static btl_2.BombermanControler.Menu.*;
import static btl_2.thucthe.doituongtinh.Portal.*;
import static btl_2.manchoi.NextLevel.*;
import static btl_2.amthanh.SoundManager.updateSound;

public class BombermanGame extends Application {

    public static final int WIDTH = 25;
    public static final int HEIGHT = 15;
    public static int bom_Width = 0;
    public static int bom_Height = 0;
    public static int bom_Level = 1;

    public static final List<Entity> vatCan = new ArrayList<>();
    public static List<DoiTuong> enemies = new ArrayList<>();
    public static int[][] checkIndex;
    public static int[][] listKills;
    public static DoiTuong doiTuong;
    public static boolean isRun;
    public static ImageView home_view;
    private GraphicsContext gC;
    private Canvas cv;
    private int frame = 1;
    private long lasttime;
    public static Stage stage = null;

    public static void main(String[] args) {
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        //tao Canvas
        cv = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        cv.setTranslateY(32);
        gC = cv.getGraphicsContext2D();
        Image back = new Image("images/back.jpg");
        home_view = new ImageView(back);
        home_view.setX(-400);
        home_view.setY(-208);
        home_view.setScaleX(0.5);
        home_view.setScaleY(0.5);
        // Tao root container
        Group root = new Group();
        Menu.createMenu(root);
        root.getChildren().add(cv);
        root.getChildren().add(home_view);
        // Tao scene
        Scene scene = new Scene(root);
        scene.setOnKeyPressed(event -> {
            if (doiTuong.isLifeOfEnemy())
                switch (event.getCode()) {
                    case UP:
                        DiChuyen.huongLen(doiTuong);
                        break;
                    case DOWN:
                        DiChuyen.huongXuong(doiTuong);
                        break;
                    case LEFT:
                        DiChuyen.huongTrai(doiTuong);
                        break;
                    case RIGHT:
                        DiChuyen.huongPhai(doiTuong);
                        break;
                    case SPACE:
                        Bomb.datBom();
                        break;
                }
        });
        // Them scene vao stage
        stage.setScene(scene);
        stage.setTitle("Java Game Bomberman");
        Image icon = new Image("images/icon.jpg");
        stage.getIcons().add(icon);
        BombermanGame.stage = stage;
        BombermanGame.stage.show();
        lasttime = System.currentTimeMillis();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                if (isRun) {
                    render();
                    update();
                    time();
                    capNhat_menu();
                }
            }
        };
        timer.start();

        doiTuong = new Bomber(1, 1, Sprite.player_right_2.getFxImage());
        doiTuong.setLifeOfEnemy(false);
    }

    public void update() {
        vatCan.forEach(Entity::update);
        enemies.forEach(Entity::update);
        doiTuong.update();

        doiTuong.setCountToRun(doiTuong.getCountToRun() + 1);
        if (doiTuong.getCountToRun() == 4) {
            DiChuyen.checkRun(doiTuong);
            doiTuong.setCountToRun(0);
        }

        for (DoiTuong a : enemies) {
            a.setCountToRun(a.getCountToRun() + 1);
            if (a.getCountToRun() == 8) {
                DiChuyen.checkRun(a);
                a.setCountToRun(0);
            }
        }

        if (enemies.size() == 0 && !isPortal && !isWait) {
            Entity portal = new Portal(bom_Width - 2, bom_Height - 2, Sprite.portal.getFxImage());
            vatCan.add(portal);
            if (doiTuong.getX() / 32 == portal.getX() / 32 && doiTuong.getY() / 32 == portal.getY() / 32) {
                isWait = true;
                timeWaitting = System.currentTimeMillis();
            }
        }
        waitToLevelUp();
        updateSound();
    }

    public void render() {
        gC.clearRect(0, 0, cv.getWidth(), cv.getHeight());
        vatCan.forEach(g -> g.render(gC));
        enemies.forEach(g -> g.render(gC));
        doiTuong.render(gC);
    }

    public void time() {
        frame++;
        long now = System.currentTimeMillis();
        if (now - lasttime > 1000) {
            lasttime = System.currentTimeMillis();
            stage.setTitle("Bomberman | " + frame + " frame");
            frame = 0;

            times.setText("Time: " + timeNumber);
            timeNumber--;
            if (timeNumber < 0)
                doiTuong.setLifeOfEnemy(false);
        }
    }
}

