package btl_2.manchoi;

import javafx.scene.image.Image;
import btl_2.thucthe.doituongdong.*;
import btl_2.dohoa.TaoMap;
import btl_2.dohoa.Sprite;
import static btl_2.BombermanGame.*;
import static btl_2.BombermanControler.Menu.*;
import static btl_2.thucthe.doituongdong.Bomber.killSwap;
import static btl_2.thucthe.doituongtinh.Bomb.is_bom;
import static btl_2.thucthe.item.SpeedItem.speed;
import static btl_2.amthanh.SoundManager.*;

public class Man2 {
    public Man2() {
        is_bom = 0;
        enemies.clear();
        vatCan.clear();
        killSwap = 1;
        new TaoMap("res/levels/Level2.txt");
        doiTuong.setLifeOfEnemy(true);
        doiTuong.setX(32);
        doiTuong.setY(32);
        speed = 1;
        isTitle = false;
        isDied = false;
        bombNumber = 30;
        timeNumber = 120;

        doiTuong.setImg(Sprite.player_right_2.getFxImage());
        Image transparent = new Image("images/transparent.png");
        home_view.setImage(transparent);

        DoiTuong e1 = new Ballom(5, 5, Sprite.ballom_left1.getFxImage());
        DoiTuong e2 = new Ballom(11, 9, Sprite.ballom_left1.getFxImage());
        enemies.add(e1);
        enemies.add(e2);

        DoiTuong e3 = new Kondoria(1, 3, Sprite.kondoria_right1.getFxImage());
        DoiTuong e4 = new Kondoria(1, 7, Sprite.kondoria_right1.getFxImage());
        DoiTuong e5 = new Kondoria(1, 11, Sprite.kondoria_right1.getFxImage());
        enemies.add(e3);
        enemies.add(e4);
        enemies.add(e5);

        DoiTuong e6 = new Oneal(7, 5, Sprite.oneal_right1.getFxImage());
        DoiTuong e7 = new Oneal(19, 7, Sprite.oneal_right1.getFxImage());
        enemies.add(e6);
        enemies.add(e7);

        for (DoiTuong doiTuong : enemies) {
            doiTuong.setLifeOfEnemy(true);
        }
    }
}
