package btl_2.manchoi;

import javafx.scene.image.Image;
import btl_2.thucthe.doituongdong.*;
import btl_2.dohoa.*;
import static btl_2.BombermanGame.*;
import static btl_2.BombermanControler.Menu.*;
import static btl_2.thucthe.doituongdong.Bomber.killSwap;
import static btl_2.thucthe.doituongtinh.Bomb.*;
import static btl_2.thucthe.item.SpeedItem.speed;
import static btl_2.amthanh.SoundManager.*;

public class Man1 {
    public Man1() {
        enemies.clear();
        vatCan.clear();
        power_Bom = 0;
        killSwap = 1;
        new TaoMap("res/levels/Level1.txt");
        doiTuong.setLifeOfEnemy(true);
        doiTuong.setX(32);
        doiTuong.setY(32);
        isTitle = false;
        isDied = false;
        bombNumber = 20;
        is_bom = 0;
        speed = 1;
        timeNumber = 120;

        doiTuong.setImg(Sprite.player_right_2.getFxImage());
        Image transparent = new Image("images/transparent.png");
        home_view.setImage(transparent);

        DoiTuong e1 = new Ballom(4, 4, Sprite.ballom_left1.getFxImage());
        DoiTuong e2 = new Ballom(9, 9, Sprite.ballom_left1.getFxImage());
        DoiTuong e3 = new Ballom(22, 6, Sprite.ballom_left1.getFxImage());
        enemies.add(e1);
        enemies.add(e2);
        enemies.add(e3);

        DoiTuong e4 = new Oneal(7, 6, Sprite.oneal_right1.getFxImage());
        DoiTuong e5 = new Oneal(13, 8, Sprite.oneal_right1.getFxImage());
        enemies.add(e4);
        enemies.add(e5);
        DoiTuong e6 = new Doll(7, 5, Sprite.doll_left1.getFxImage());
        enemies.add(e6);
        DoiTuong e7 = new Kondoria(1, 13, Sprite.kondoria_right1.getFxImage());
        enemies.add(e7);

        for (DoiTuong doiTuong : enemies) {
            doiTuong.setLifeOfEnemy(true);
        }
    }
}