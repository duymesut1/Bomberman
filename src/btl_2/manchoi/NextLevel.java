package btl_2.manchoi;

import javafx.scene.image.Image;

import static btl_2.BombermanGame.*;
import static btl_2.thucthe.doituongtinh.Portal.isPortal;

public class NextLevel {
    public static boolean isWait;
    public static long timeWaitting;

    public static void waitToLevelUp() {
        if (isWait) {
            Image waitToNext = new Image("images/levelup.jpg");
            home_view.setImage(waitToNext);
            long timeNow = System.currentTimeMillis();
            if (timeNow - timeWaitting > 3000) {
                switch (bom_Level) {
                    case 1:
                        isPortal = false;
                        new Man2();
                        break;
                    case 2:
                        new Man3();
                        break;
                    case 3:
                        new Man1();
                }
                isWait = false;
            }
        }
    }
}
