package btl_2.manchoi;

import javafx.scene.image.Image;
import btl_2.thucthe.doituongdong.DoiTuong;
import btl_2.thucthe.doituongdong.Ballom;
import btl_2.thucthe.doituongdong.Doll;
import btl_2.dohoa.TaoMap;
import btl_2.dohoa.Sprite;

import static btl_2.BombermanGame.*;
import static btl_2.BombermanGame.enemies;
import static btl_2.BombermanControler.Menu.bombNumber;
import static btl_2.BombermanControler.Menu.timeNumber;
import static btl_2.thucthe.doituongdong.Bomber.killSwap;
import static btl_2.thucthe.doituongtinh.Bomb.is_bom;
import static btl_2.thucthe.item.SpeedItem.speed;
import static btl_2.amthanh.SoundManager.isDied;
import static btl_2.amthanh.SoundManager.isTitle;

public class Man3 {
    public Man3() {
        enemies.clear();
        vatCan.clear();
        killSwap = 1;
        new TaoMap("res/levels/Level3.txt");
        doiTuong.setLifeOfEnemy(true);
        doiTuong.setX(32);
        doiTuong.setY(32);
        speed = 1;
        isDied = false;
        isTitle = false;
        timeNumber = 120;
        bombNumber = 40;
        is_bom = 0;

        doiTuong.setImg(Sprite.player_right_2.getFxImage());
        Image transparent = new Image("images/transparent.png");
        home_view.setImage(transparent);

        DoiTuong e1 = new Ballom(5, 5, Sprite.ballom_left1.getFxImage());
        DoiTuong e2 = new Ballom(11, 9, Sprite.ballom_left1.getFxImage());
        enemies.add(e1);
        enemies.add(e2);

        DoiTuong e3 = new Doll(7, 5, Sprite.doll_left1.getFxImage());
        enemies.add(e3);

        for (DoiTuong doiTuong : enemies) {
            doiTuong.setLifeOfEnemy(true);
        }
    }
}
