package btl_2.dohoa;

import btl_2.thucthe.Entity;
import btl_2.thucthe.doituongtinh.*;
import btl_2.thucthe.item.*;

import static btl_2.BombermanGame.*;

import java.io.*;
import java.util.*;

public class TaoMap {
    public TaoMap(String level) {
        final File fileName = new File(level);
        try (FileReader inputFile = new FileReader(fileName)) {
            Scanner sc = new Scanner(inputFile);
            String line = sc.nextLine();

            StringTokenizer tokens = new StringTokenizer(line);
            bom_Level = Integer.parseInt(tokens.nextToken());
            bom_Height = Integer.parseInt(tokens.nextToken());
            bom_Width = Integer.parseInt(tokens.nextToken());

            while (sc.hasNextLine()) {
                checkIndex = new int[bom_Width][bom_Height];
                listKills = new int[bom_Width][bom_Height];
                for (int i = 0; i < bom_Height; ++i) {

                    String lineTile = sc.nextLine();
                    StringTokenizer tokenTile = new StringTokenizer(lineTile);

                    for (int j = 0; j < bom_Width; j++) {
                        int s = Integer.parseInt(tokenTile.nextToken());
                        Entity e;
                        switch (s) {
                            case 1:
                                e = new Portal(j, i, Sprite.grass.getFxImage());
                                s = 0;
                                break;
                            case 2:
                                e = new Wall(j, i, Sprite.wall.getFxImage());
                                break;
                            case 3:
                                e = new Brick(j, i, Sprite.brick.getFxImage());
                                break;
                            case 6:
                                e = new SpeedItem(j, i, Sprite.brick.getFxImage());
                                break;
                            case 7:
                                e = new FlameItem(j, i, Sprite.brick.getFxImage());
                                break;
                            default:
                                e = new Grass(j, i, Sprite.grass.getFxImage());
                        }
                        checkIndex[j][i] = s;
                        vatCan.add(e);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
